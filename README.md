# Ground Control Discord

## About

The Ground Control Discord bot is a bot that connects to Discord channels and to a (Satellite) Server. It then relays the contents of the connected Server channels to and from the respective Discord channels.
Simply put:
> Bot take things from Discord and put them to Satellite Server. Bot take things from Stallite Server and put them in Discord.


## System

The programm was developed in Python 3.8.10 on Linux (Ubuntu 20.04 LTS). When setting up the system the Linux distribution should probably not matter but the Python version should be at least 3.8.10 (no lower versions have been tested).


## Setup

1. Clone the repository and it's submodules with either SSH:
    ```sh
    git clone --recurse-submodules git@gitlab.com:remote_exploration_simulation/ground_control_discord.git
    ```
    Or HTTPs:
    ```sh
    git clone --recurse-submodules https://gitlab.com/remote_exploration_simulation/ground_control_discord.git
    ```

2. Enter repository directory. Then initialize a virtual environment and enter it:
    ```sh
    cd ground_control_discord
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Install required packages
    ```sh
    pip3 install -r requirements.txt
    ```

4. Create `.env` file to store the server ip and port, the discord token and the channels of the server available to the bot. You can also add the variable for the id of the server role with top level permissions (binding, unbinding, etc.).
    ```sh
    touch .env
    echo "SATELLITE_IP = <Server IPv4>" >> .env
    echo "SATELLITE_PORT = <Server port number>" >> .env
    echo "SATELLITE_TOKENS = <Your comma separated channel tokens>" >> .env
    echo "DISCORD_TOKEN = <Your token from the Discord developers site>" >> .env
    echo "SUPER_ROLE = <User role on your server with top level permissions>" >> .env
    ```


## Starting the bot

In order to start the bot simply run
```sh
python3 src/main.py
```


## Stopping the bot

The bot runs indefinitely and can, for now, only be forcefully stopped by pressing `Ctrl`+`C`.


class ClientAlreadyBoundError(Exception):
    """ Tried to bind to client that another channel is already bound to """
    def __init__(self, channelBoundOnClient, msg=''):
        self.channelBoundOnClient = channelBoundOnClient
        super().__init__(msg)


class ClientDoesNotExistError(Exception):
    """ Tried to use a network client that does not exist """
    pass


class ChannelDoesNotExistError(Exception):
    """ Tried to use a discord channel that does not exist """
    pass


class ClientAlreadyExistsError(Exception):
    """ Tried to add a network client that already exists """
    pass


class ChannelAlreadyBoundError(Exception):
    """ Tried to bind a channel that is already bound to a client """
    def __init__(self, clientChannelIsBoundTo, msg=''):
        self.clientChannelIsBoundTo = clientChannelIsBoundTo
        super().__init__(msg)

import asyncio
import environs
import logging
import sys
import tempfile

from bot.DiscordBot import DiscordBot
from network_conn.NetworkClient import NetworkClient


def getServerDataOrExit():
    env = environs.Env()
    env.read_env()
    try:
        ip = env('SATELLITE_IP')
    except environs.EnvError:
        print('Set environment variable SATELLITE_IP in order to start!', file=sys.stderr)
        sys.exit(1)

    try:
        port = env('SATELLITE_PORT')
    except environs.EnvError:
        print('Set environment variable SATELLITE_PORT in order to start!', file=sys.stderr)
        sys.exit(1)

    try:
        tokens = env.list('SATELLITE_TOKENS', subcast=str, delimiter=',')
    except environs.EnvError:
        print('Set environment variable SATELLITE_TOKENS in order to start!', file=sys.stderr)
        sys.exit(1)

    return ip, port, tokens


def getDiscordTokenOrExit():
    env = environs.Env()
    env.read_env()

    try:
        token = env('DISCORD_TOKEN')
    except environs.EnvError:
        print('Set environment variable DISCORD_TOKEN in order to start!', file=sys.stderr)
        sys.exit(1)

    return token


def getDiscordSuperRoleID(logger):
    env = environs.Env()
    env.read_env()

    try:
        superuserRoleName = env.int('SUPER_ROLE')
    except environs.EnvError:
        superuserRoleName = None
        logger.warn(r'No SUPER_ROLE environment variables found or content is '
                    r'not of type int. Every user has superuser permissions!')

    return superuserRoleName


def getAllEnvVars(logger):
    satIP, satPort, satTokens = getServerDataOrExit()
    discordToken = getDiscordTokenOrExit()
    superRoleID = getDiscordSuperRoleID(logger)

    return satIP, satPort, satTokens, discordToken, superRoleID


async def startSystem():
    # Configure logging
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)s: %(message)s',
            handlers=[handler])
    logger = logging.getLogger(__name__)

    satIP, satPort, satTokens, discordToken, superRoleID = getAllEnvVars(logger)

    # Create temp. dir. for buffering images/files between client and bot
    tmpDir = tempfile.TemporaryDirectory(prefix='ground_control_buffer-')

    try:
        url = f'ws://{satIP}:{satPort}'
        networkClients = [NetworkClient(url, tkn, tmpDir.name, logger) for tkn in satTokens]
        networkClientsTasks = [client.start() for client in networkClients]

        bot = DiscordBot(logger, networkClients, superRoleID)
        discordTask = bot.start(discordToken)

        tasks = networkClientsTasks + [discordTask]
        await asyncio.gather(*tasks)

    finally:
        tmpDir.cleanup()
        logger.info('Cleaned up buffer directory.')
        logger.info('Exiting.')


if __name__ == '__main__':
    asyncio.run(startSystem())

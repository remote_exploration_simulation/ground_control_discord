import asyncio
import discord
import os
import sys

from bot.Bindings import Bindings
import bot.discordMessaging as discordMsg
import errors.bindingErrors as bindErr
import internal_comm.internalMessages as interMsg

superPrefix = '!'
summonCommand = 'bind'
disableCommand = 'unbind'
listNetClientsCommand = 'list'
statusCommand = 'status'
purgeCommand = 'purge'


class DiscordBot(discord.Client):

    def __init__(self, logger, networkClients, superRoleID):
        super().__init__()
        self.logger = logger
        self.bindings = Bindings(networkClients)
        self.superRoleID = superRoleID
        self.networkClients = networkClients


    async def start(self, token):
        tasks = [super().start(token)]
        for c in self.networkClients:
            tasks.append(self.consumeFromClient(c))
        await asyncio.gather(*tasks)


    async def on_ready(self):
        self.logger.info(f'Connected to the guilde as {self.user}.')


    async def on_message(self, message):
        if message.author == self.user:
            return

        if message.content.startswith(superPrefix):
            if not type(message.author) is discord.Member:
                await discordMsg.sendSuperError(
                        message.channel,
                        'User not member of server!')
            elif not self.superRoleID in ([r.id for r in message.author.roles] + [None]):
                await discordMsg.sendSuperError(
                        message.channel,
                        'Missing permissions!')
            else:
                await self.processCommandMessage(message)
            return

        client = self.bindings.getBoundClientOnChannel(message.channel)

        if client is None:
            return

        if not client.connected:
            await discordMsg.sendSuperWarn(
                    message.channel,
                    'Bound to channel but no connection to Sattelite Server! '\
                    'Message was not sent!')
            return

        if len(message.content) == 0:
            await discordMsg.sendSuperWarn(
                    message.channel,
                    'Message can not be an image or empty! '\
                    'Message was not sent!')
            return

        await client.inQueue.put(message.content)
        await message.channel.send("sending")


    async def processCommandMessage(self, message):
        argv = message.content.split()
        if argv[0] == superPrefix + statusCommand:
            ret = f'Channel #{message.channel}: '
            client = self.bindings.getBoundClientOnChannel(message.channel)
            if client is None:
                ret += 'not bound'
            else:
                ret += f'bound to {client.name}'

            ret += '\n\nAll clients:\n'
            ret += str(self.bindings)
            await discordMsg.sendSuperInfo(message.channel, ret)

        elif argv[0] == superPrefix + listNetClientsCommand:
            ret = 'Available clients:\n'
            ret += str(self.bindings)
            await discordMsg.sendSuperInfo(message.channel, ret)

        elif argv[0] == superPrefix + summonCommand:
            if len(argv) != 2:
                errMsg = f'Usage: {superPrefix+summonCommand} <client name>'
                await discordMsg.sendSuperError(message.channel, errMsg)
                return
            await self.tryBindChannel(message.channel, argv[1])

        elif argv[0] == superPrefix + disableCommand:
            await self.unbindChannel(message.channel)

        elif argv[0] == superPrefix + purgeCommand:
            if len(argv) != 2:
                errMsg = f'Usage: {superPrefix+purgeCommand} <limit>'
                await discordMsg.sendSuperError(message.channel, errMsg)
                return
            await self.tryPurgeMessages(message.channel, argv[1])

        else:
            await discordMsg.sendSuperError(message.channel, 'Unknown command!')


    async def tryPurgeMessages(self, channel, limit):
        if not limit.isnumeric():
            await discordMsg.sendSuperError(channel, 'Limit has to be an integer! ')
            return

        limitInt = int(limit)

        # Purging is extremely slow due to the rate limiting of Discord.
        # Purging large numbers of messages leads to a very delayed and
        # unintuitive experience for the user. Therefore it is forbidden!
        if limitInt > 10:
            await discordMsg.sendSuperError(channel, 'Limit has to be < 11!')
            return

        try:
            await channel.purge(limit=limitInt+1, bulk=True)
        except discord.HTTPException as e:
            await discordMsg.sendSuperError(
                    channel,
                    'Purging failed! (Check logs for more information)')
            self.logger.warn(
                    f'Purging failed! HTTP status: {e.status}, '\
                    f'Discord error code: {e.code}, '\
                    f'Error message: {e.text}')


    async def tryBindChannel(self, channel, clientName):
        client = self.bindings.getClientFromName(clientName)

        if client is None:
            # No client found
            await discordMsg.sendSuperError(channel, 'Client does not exist!')
            return

        if channel == self.bindings.getBoundChannelOnClient(client):
            # Already bound to the channel
            await discordMsg.sendSuperError(
                    channel,
                    'Already bound to this channel!')
            return

        try:
            # Try to bind the channel to a client
            self.bindings.bindChannelToClient(channel, client)
            await discordMsg.sendSuperInfo(
                    channel,
                    'Bound to channel. Ready to use!')

        except bindErr.ClientAlreadyBoundError as e:
            # There is aready a channel bound to the client
            blockingChannel = e.channelBoundOnClient
            msg = f'Binding failed! The channel #{blockingChannel.name} '\
                  f'is already bound to this client.'
            await discordMsg.sendSuperError(channel, msg)

        except bindErr.ClientDoesNotExistError:
            # The client does not exist
            msg = f'Binding failed! The client {clientName} does not exist.'
            await discordMsg.sendSuperError(channel, msg)

        except bindErr.ChannelAlreadyBoundError as e:
            # The channel is already bound to a client
            clientName = e.clientChannelIsBoundTo.token[:5]
            msg = f'Binding failed! The channel is already bound to {clientName}.'
            await discordMsg.sendSuperError(channel, msg)


    async def unbindChannel(self, channel):
        try:
            self.bindings.unbindChannel(channel)
            await discordMsg.sendSuperInfo(channel, 'Channel unbound!')

        except bindErr.ChannelDoesNotExistError:
            await discordMsg.sendSuperWarn(channel, 'Channel not bound!')


    # Gets started for each Network Client and runs "concurrently"
    async def consumeFromClient(self, netClient):
        while True:
            message = await netClient.outQueue.get()

            targetChannel = self.bindings.getBoundChannelOnClient(netClient)

            if targetChannel is None:
                continue

            if type(message) is interMsg.IMControl:
                await self.consumeFromClientCtrl(message, targetChannel)
            elif type(message) is interMsg.IMText:
                await self.consumeFromClientTxt(message, targetChannel)
            elif type(message) is interMsg.IMImage:
                await self.consumeFromClientImg(message, targetChannel)
            else:
                self.logger.error(
                        f'Message in Network Client outQueue of illegal '\
                        f'type: {str(type(message))}! Message was dropped. '\
                        f'Message content was:\n{str(message)}')


    async def consumeFromClientCtrl(self, msg, targetChannel):
        self.logger.info(
                f'Received internal control message from '\
                f'[{msg.senderID} {msg.senderType}], of type '\
                f'{str(msg.ctrlType)} with content "{str(msg.args)}"')


    async def consumeFromClientTxt(self, msg, targetChannel):
        # If the message is larger than 2000 (characters?) discord raises a
        # 400 warning, that the message is too long. Therefore, if the message
        # is too long it is cut short to a little shorter than 2000 characters
        # to still allow for some decoration around the text. A warning is
        # send too, to inform the user
        await discordMsg.sendStdInfo(targetChannel, msg.text[:1950])
        if len(msg.text) > 1950:
            await discordMsg.sendStdWarn(
                    targetChannel,
                    'Message was longer than 1950 characters and got cut off.')


    async def consumeFromClientImg(self, msg, targetChannel):
        try:
            await discordMsg.sendStdImg(targetChannel, msg.imgPath)
            os.remove(msg.imgPath)
        except FileNotFoundError:
            self.logger.warn(f'Nonexisted image path received from client!')
            await discordMsg.sendSuperWarn(
                    channel=targetChannel,
                    message='Error when receiving image!')

import discord


async def sendSuperInfo(channel, message):
    embed=discord.Embed(
            title='**info**',
            description='```\n' + message + '\n```',
            color=discord.Color.blue())
    await channel.send(embed=embed)


async def sendSuperWarn(channel, message):
    embed=discord.Embed(
            title='**warning**',
            description='```\n' + message + '\n```',
            color=discord.Color.orange())
    await channel.send(embed=embed)


async def sendSuperError(channel, message):
    #message = '**error**\n```diff\n- ' + message + '\n```'
    embed=discord.Embed(
            title='**error**',
            description='```\n' + message + '\n```',
            color=discord.Color.red())
    await channel.send(embed=embed)


async def sendStdInfo(channel, message):
    message = '```\n' + message + '\n```'
    await channel.send(message)
    

async def sendStdWarn(channel, message):
    message = '**warning**\n```\n' + message + '\n```'
    await channel.send(message)


async def sendStdImg(channel, imgPath):
    with open(imgPath, 'rb') as f:
        img = discord.File(f)
        await channel.send(file=img)

import errors.bindingErrors as errors

class Bindings:
    """
    Each network client can be connected to 1 Discord channels. And every
    Discord channel can be connected to 1 network client.
    """

    def __init__(self, clients=[]):
        self.clientsNames = {}
        self.networkClients = {}
        self.discordChannels = {}

        for client in clients:
            self.addNetworkClient(client)


    def __str__(self):
        ret = ''
        for client in self.networkClients:
            ret += str(client.token[:5]).ljust(6, ' ')
            ret += '1 ' if client.connected else '0 '
            if self.networkClients[client] is None:
                ret += '-'
            else:
                ret += self.networkClients[client].name
            ret += '\n'

        return ret


    def bindChannelToClient(self, discordChannel, client):
        if not self.networkClients[client] is None:
            raise errors.ClientAlreadyBoundError(self.networkClients[client])

        if not client in self.networkClients:
            raise errors.ClientDoesNotExistError()

        if discordChannel in self.discordChannels:
            raise errors.ChannelAlreadyBoundError(self.discordChannels[discordChannel])

        # Add to networkClients
        self.networkClients[client] = discordChannel
        client.bind(discordChannel)

        # Add to discordChannels
        self.discordChannels[discordChannel] = client


    def unbindChannel(self, discordChannel):
        if not discordChannel in self.discordChannels:
            raise errors.ChannelDoesNotExistError()

        # Remove from networkClients
        for client in self.networkClients:
            if discordChannel == self.networkClients[client]:
                self.networkClients[client] = None
                client.unbind()

        # Remove from discordChannels
        del(self.discordChannels[discordChannel])



    def addNetworkClient(self, client):
        if client in self.networkClients:
            raise errors.ClientAlreadyExistsError()

        self.networkClients[client] = None

        name = client.token[:5]
        self.clientsNames[name] = client


    def getBoundChannelOnClient(self, client):
        return self.networkClients.get(client, None)


    def getBoundClientOnChannel(self, discordChannel):
        return self.discordChannels.get(discordChannel, None)


    def getClientFromName(self, clientName):
        return self.clientsNames.get(clientName, None)

class InternalMessage:
    def __init__(self, senderID, senderType):
        self.senderID = senderID
        self.senderType = senderType


class IMText(InternalMessage):
    def __init__(self, senderID, senderType, text):
        super().__init__(senderID, senderType)
        self.text = text


class IMImage(InternalMessage):
    def __init__(self, senderID, senderType, imgPath):
        super().__init__(senderID, senderType)
        self.imgPath = imgPath


class IMControl(InternalMessage):
    def __init__(self, senderID, senderType, ctrlType, args):
        super().__init__(senderID, senderType)
        self.ctrlType = ctrlType
        self.args = args

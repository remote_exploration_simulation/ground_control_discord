import asyncio
import aiofiles
import websockets as ws
import os
import random

import satellite_network_protocol.src.package as package
from satellite_network_protocol.src.package import PAYLOAD_SIZE
import satellite_network_protocol.src.utilities as snpUtils
import internal_comm.internalMessages as interMsg


class Buffer:
    def __init__(self, contentType, senderType):
        self.contentType = contentType
        self.senderType = senderType
        self.content = []
        self.receivedLast = False


    def isMessageComplete(self):
        if not self.receivedLast:
            return False

        return None not in self.content


    def addToContent(self, number, isLast, content):
        if len(self.content) <= number:
            self.content.extend([None for _ in range(number - len(self.content) + 1)])

        self.content[number] = content

        self.receivedLast = self.receivedLast or isLast


    def getFullContent(self):
        flattened = b''
        for b in self.content:
            flattened += b

        return flattened


class MessageQueue(asyncio.Queue):
    def __init__(self, maxSize):
        super().__init__(maxSize)


    def flush(self):
        while True:
            try:
                internMsg = self.get_nowait()
                if type(internMsg) is interMsg.IMImage:
                    try:
                        os.remove(interMsg.imgPath)
                    except OSError:
                        pass
                else:
                    del interMsg
            except asyncio.QueueEmpty:
                break

class MessageIdCreator:
    def __init__(self):
        self.currentId = random.randint(0, 255)

    def getNewId(self):
        old = self.currentId
        new = (old + 1) % 256
        self.currentId = new

        return old



class NetworkClient():

    def __init__(self, url, token, bufferDir, logger):
        self.maxQueueSize = 512

        self.socket = None
        self.id = 0
        self.token = token
        self.url = url
        self.inQueue = MessageQueue(self.maxQueueSize)
        self.outQueue = MessageQueue(self.maxQueueSize)
        self.bufferDir = bufferDir
        self.logger = logger

        self.connected = False
        self.name = token[:5]
        self.boundChannel = None
        self.consumerBuffer = {}

        self.messageIdCreator = MessageIdCreator()


    async def start(self):
        while True:
            try:
                async with ws.connect(self.url) as conn:
                    await self.runConnected(conn)

            except ConnectionRefusedError:
                self.connected = False
                self.logger.error(
                        r'Could not connect to server! '
                        r'Server may be offline or the IP-adress might be wrong. '
                        r'(Trying to reconnect in 10s)')
                await asyncio.sleep(10)

            except ws.exceptions.ConnectionClosedError:
                self.connected = False
                self.logger.error(
                    'Connection to server lost! (Trying to reconnect in 10s)')
                await asyncio.sleep(10)


    async def runConnected(self, conn):
        # Send initial connection request
        await conn.send(snpUtils.buildCtrlConnReq(self.token))

        # Get either own ID or get connection refused message from server
        msg = await conn.recv()
        pkg = package.Package(msg)
        self.consumeCtrlPkg(pkg)

        if not self.connected:
            return

        self.logger.info('Handshake with server successfull!')
        await asyncio.gather(
            self.producer(conn),
            self.consumer(conn),)


    async def producer(self, conn):
        self.logger.debug('Producer started.')
        while True:
            # Wait for element in inQueue
            msg = await self.inQueue.get()

            # Send message
            if type(msg) is str:
                await self.sendText(msg, conn)
            else:
                self.logger.warn(
                    fr'Message "{str(msg)}" was not of type string '
                    fr'and was not sent!')


    async def consumer(self, conn):
        self.logger.debug('Consumer started.')
        async for retMsg in conn:
            pkg = package.Package(retMsg)
            self.consumePackage(pkg)

            if not self.connected:
                return


    def consumePackage(self, pkg):
        if pkg.isControl:
            self.consumeCtrlPkg(pkg)
        else:
            self.consumeStdPkg(pkg)


    # For bot

    def bind(self, channel):
        self.boundChannel = channel


    def unbind(self):
        self.boundChannel = None
        self.outQueue.flush()
        self.inQueue.flush()


    # Sending

    async def sendText(self, msgStr, conn):
        msgBytes = bytes(msgStr, 'utf-8')
        n = PAYLOAD_SIZE
        segments = [msgBytes[i:i+n] for i in range(0, len(msgBytes), n)]

        for i in range(len(segments)):
            package = snpUtils.buildStdPackage(
                    payload = segments[i],
                    payloadType = 'txt',
                    senderID = self.id,
                    senderType = 'ground',
                    messageID = self.messageIdCreator.getNewId(),
                    number = i,
                    last = (i == len(segments) - 1))
            await conn.send(package)


    # Receiving

    def tryPutIntoOutQueue(self, obj):
        if self.boundChannel is None:
            return

        try:
            self.outQueue.put_nowait(obj)
        except asyncio.QueueFull:
            self.logger.warn('Output queue full. Bot might have disconnected incorrectly or might be too slow!')
            pass


    def receivedTxt(self, txt, senderID, senderType):
        im = interMsg.IMText(senderID, senderType, txt)
        self.tryPutIntoOutQueue(im)

        self.logger.debug(f'[{senderID} ({senderType})] txt: {txt}')


    def receivedImg(self, imgPath, senderID, senderType):
        im = interMsg.IMImage(senderID, senderType, imgPath)
        self.tryPutIntoOutQueue(im)
        self.logger.debug(f'[{senderID} ({senderType})] img: Buffered image to "{imgPath}"')


    # Consuming

    def consumeCtrlPkg(self, pkg):
        if not pkg.isControl:
            return

        # If package is not from the server cancel
        if pkg.senderID != 0:
            return

        payload = pkg.payload

        # Connection refused from server
        if payload[0] == 0:
            # Close connection
            self.logger.info(
                    f'Connection was refused with message: {pkg.payload[1:]}')
            self.connected = False

        # Receive new ID
        elif payload[0] == 1:
            self.connected = True
            self.id = int.from_bytes(payload[1:3], byteorder='big')
            self.logger.info(f'Received new id: {str(self.id)}')


    def consumeStdPkg(self, pkg):
        key = (pkg.senderID, pkg.messageID)
        # If incorrect payload type in buffer reset it
        senderBuffer = self.consumerBuffer.get(key, None)
        if senderBuffer is None or senderBuffer.contentType != pkg.payloadType:
            self.consumerBuffer[key] = Buffer(pkg.payloadType, pkg.senderType)
            self.logger.info('Only partial package received. Got dropped.')

        self.consumerBuffer[key].addToContent(
                number=pkg.number,
                isLast=pkg.isLast,
                content=pkg.payload)

        if self.consumerBuffer[key].isMessageComplete():
            if self.consumerBuffer[key].contentType == 'txt':
                contentAsBytes = self.consumerBuffer[key].getFullContent()
                text = contentAsBytes.decode('utf-8')
                self.receivedTxt(text, pkg.senderID, pkg.senderType)

            elif self.consumerBuffer[key].contentType == 'img':
                contentAsBytes = self.consumerBuffer[key].getFullContent()
                imgPath = self.getImgPath(pkg.senderID)
                with open(imgPath, 'wb') as fl:
                    fl.write(contentAsBytes)
                self.receivedImg(imgPath, pkg.senderID, pkg.senderType)

            else:
                self.logger.warn('Received package of unknow type. Got dropped.')

            self.consumerBuffer.pop(key)


    def getImgPath(self, senderID):
        return os.path.join(self.bufferDir, 'img_' + str(senderID) + '.jpg')
